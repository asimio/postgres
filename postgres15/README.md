### Usage example:

- To start a container with an existing database.

```bash
docker run -d -e DB_NAME=db_dvdrental -e DB_USER=user_dvdrental -e DB_PASSWD=changeit asimio/postgres15
```

- To start a container and create a new database.

```bash
docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e DB_NAME=db_newdb -e DB_USER=user_newdb -e DB_PASSWD=changeit asimio/postgres15:latest
```

### Extending it:
```bash
FROM asimio/postgres15

COPY sql/* /tmp/data/db_dvdrental
# Seems scripts will get executed in alphabetical-sorted order, db-init.sh needs to be executed first
ADD scripts/db-setup.sh /docker-entrypoint-initdb.d/
RUN chmod 755 /docker-entrypoint-initdb.d/db-setup.sh
```

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero