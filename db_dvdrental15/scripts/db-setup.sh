#!/bin/bash

echo "Setting up and inserting data into DB $DB_NAME"
psql -U $POSTGRES_USER -d $DB_NAME -a -f /tmp/data/db_dvdrental/pagila-schema.sql
psql -U $POSTGRES_USER -d $DB_NAME -a -f /tmp/data/db_dvdrental/pagila-data.sql
echo "$DB_NAME seeded with initial data"

echo "Granting permissions in DB '$DB_NAME' to role '$DB_USER'."
psql -v ON_ERROR_STOP=on -U $POSTGRES_USER -d $DB_NAME <<-EOSQL
  GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO $DB_USER;
  GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO $DB_USER;
  -- FIXME FUNCTIONS TRIGGERS VIEWS
EOSQL
echo "Permissions granted"
