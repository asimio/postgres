### Usage example:

- To start a container with an existing database.

```bash
docker run -d asimio/db_dvdrental15
```
or
```bash
docker run -d -e DB_PASSWD=changeit asimio/db_dvdrental15
```
or
```
docker run -d -p 5432:5432 -e DB_NAME=db_dvdrental -e DB_USER=user_dvdrental -e DB_PASSWD=changeit asimio/db_dvdrental15:latest
```

- To start a container and create a new database.

```bash
docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e DB_NAME=db_dvdrental -e DB_USER=user_dvdrental -e DB_PASSWD=changeit asimio/db_dvdrental15:latest
```

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero