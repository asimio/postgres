### Usage example:

- To start a container with an existing database.

```bash
docker run -d -e DB_NAME=db_dvdrental -e DB_USER=user_dvdrental -e DB_PASSWD=changeit asimio/postgres
```

- To start a container and create a new database.

```bash
docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e DB_NAME=db_newdb -e DB_USER=user_newdb -e DB_PASSWD=changeit asimio/postgres:latest
```

### Extending it:
```bash
FROM asimio/postgres

RUN mkdir -p /docker-entrypoint-initdb.d
ADD sql/dvdrental.tar /docker-entrypoint-initdb.d/
```

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero